package ru.t1.nkiryukhin.tm.api.controller;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void createTask();

    void showTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
