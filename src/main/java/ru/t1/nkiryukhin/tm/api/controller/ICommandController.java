package ru.t1.nkiryukhin.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showHelp();

}
