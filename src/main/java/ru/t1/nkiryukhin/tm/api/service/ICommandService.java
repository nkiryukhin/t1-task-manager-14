package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
